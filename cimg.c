#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "base64_encode.h"

// Format spec can be found at: https://www.iterm2.com/documentation-images.html

int main(int argc, char **argv){
	FILE *input_file;
	long input_length;
	unsigned char *input_buffer, *output_buffer;

	if(argc != 2){
		printf("Usage: %s <filename>\n", argv[0]);
		return 1;
	}

	input_file = fopen(argv[1], "rb");
	if(input_file == NULL){
		printf("Could not open file %s.\n", argv[1]);
		return 1;
	}

	fseek(input_file, 0, SEEK_END); // Seek to end of file
	input_length = ftell(input_file); // Get offset
	fseek(input_file, 0, SEEK_SET); // Rewind
	
	if(input_length > (size_t)(-1)){ // Check if input fits into a buffer of max size_t
		fclose(input_file);
		printf("File is to large.\n");
		return 1;
	}

	input_buffer = malloc(input_length);

	if(input_buffer == NULL){
		fclose(input_file);
		printf("Could not allocate memory.\n");
		return 1;
	}

	fread(input_buffer, 1, input_length, input_file); // Read entire file into buffer

	fclose(input_file);

	output_buffer = base64_encode(input_buffer, input_length, NULL);
	
	if(output_buffer == NULL){
		free(input_buffer);
		printf("Base64 Encoding failed.\n");
		return 1;
	}

	printf("\033]1337;File=size=%ld;inline=1:%s\a\n", input_length, output_buffer);

	free(input_buffer);
	free(output_buffer);
	
	return 0;
}
